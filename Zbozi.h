#include <iostream>
#include "Planeta.h"

using namespace std;

class Zbozi {

private:
	float cenaZb;
	int vahaZb;

public:
	Zbozi(float cenaZb, int vahaZb);
    float getCenaZb();
    int getVahaZb();
};
