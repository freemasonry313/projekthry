#include <string>
#include <iostream>
#include "Hra.h"

using namespace std;

class Planeta {
private:
	string nazev;

public:
	Planeta(string nazev);
    string getNazev();
    void kupZb(Zbozi* zbozi);
    void prodejZb(Zbozi* zbozi);
};
