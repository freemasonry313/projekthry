#include <string>
#include <iostream>
#include "Planeta.h"
#include "Piraty.h"
#include "Hra.h"

using namespace std;

class Lod {

private:
	string nazev;
	int limitNakladu;
	float hp;
	float penize;
	float obrana;
	float utok;

public:
	Lod(string nazev, int limitNakladu, float hp, float penize, float obrana, float utok);
    string getNazev();
    float getLimitNakladu();
    float getHp();
	float getPenize();
	float getObrana();
	void printInfo();
	float getUtok();
};
